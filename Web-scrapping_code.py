
import requests
import matplotlib.pyplot as plt
from bs4 import BeautifulSoup
import pandas as pd
import numpy as np

#Page 1
response = requests.get('https://www.top500.org/list/2018/11/')                                                             #URL from which the data has to be extracted - Page1
soup = BeautifulSoup(response.text, 'html.parser')                                                                          #creates a parse tree to extract data from HTML

final = []                                                                                                                  #List in which all the data from the URL is extracted and saved                
for record in soup.findAll('tr'):                                                                                           #loop to traverse in all the rows
    tbltxt = []                                                                                                             #list to store the data of the rows
    for data in record.findAll('td'):                                                                                       #loop to traverse through the data of a particular a row
        tbltxt.append(data.text.replace('\n', ''))                                                                          
    final.append(tbltxt)
    
df1 = pd.DataFrame(final, columns=['Rank', 'Site', 'System', 'Cores', 'Rmax-TFlop', 'Rpeak-TFlop/s', 'Power-kW'])           #creating a data frame for the extracted data set that is currently stored in the final list

#Page 2
response = requests.get('https://www.top500.org/list/2018/11/?page=2')                                                      #Above written code has been repeated for the other pages of that URL 
soup = BeautifulSoup(response.text, 'html.parser')

for record in soup.findAll('tr'):
    tbltxt = []
    for data in record.findAll('td'):
        tbltxt.append(data.text.replace('\n', ''))
    final.append(tbltxt)
df2 = pd.DataFrame(final, columns=['Rank', 'Site', 'System', 'Cores', 'Rmax-TFlop', 'Rpeak-TFlop/s', 'Power-kW'])

#Page 3
response = requests.get('https://www.top500.org/list/2018/11/?page=3')
soup = BeautifulSoup(response.text, 'html.parser')

for record in soup.findAll('tr'):
    tbltxt = []
    for data in record.findAll('td'):
        tbltxt.append(data.text.replace('\n', ''))
    final.append(tbltxt)
df3 = pd.DataFrame(final, columns=['Rank', 'Site', 'System', 'Cores', 'Rmax-TFlop', 'Rpeak-TFlop/s', 'Power-kW'])

#Page 4
response = requests.get('https://www.top500.org/list/2018/11/?page=4')
soup = BeautifulSoup(response.text, 'html.parser')

for record in soup.findAll('tr'):
    tbltxt = []
    for data in record.findAll('td'):
        tbltxt.append(data.text.replace('\n', ''))
    final.append(tbltxt)
df4 = pd.DataFrame(final, columns=['Rank', 'Site', 'System', 'Cores', 'Rmax-TFlop', 'Rpeak-TFlop/s', 'Power-kW'])

#Page 5
response = requests.get('https://www.top500.org/list/2018/11/?page=5')
soup = BeautifulSoup(response.text, 'html.parser')

for record in soup.findAll('tr'):
    tbltxt = []
    for data in record.findAll('td'):
        tbltxt.append(data.text.replace('\n', ''))
    final.append(tbltxt)
df5 = pd.DataFrame(final, columns=['Rank', 'Site', 'System', 'Cores', 'Rmax-TFlop', 'Rpeak-TFlop/s', 'Power-kW'])

dfs = pd.concat([df1,df2,df3,df4,df5]).drop_duplicates().reset_index(drop=True)                                             ##Concatenation of all the data from the 5 different pages, along with removal of the duplicate values            

#Question 2 - b part : Data Cleaning a
print("Data types of columns before converting their data type")
print(dfs.dtypes)                                                                                                           ##to check the data types of all the columns in the data set

dfs["Cores"] = dfs["Cores"].str.replace(",","").astype(float)                                                               #Cleaning of data by removing commas from the digits, and conversion of the data type to float 
dfs["Rmax-TFlop"] = dfs["Rmax-TFlop"].str.replace(",","").astype(float)
dfs["Rpeak-TFlop/s"] = dfs["Rpeak-TFlop/s"].str.replace(",","").astype(float)
dfs["Power-kW"] = pd.to_numeric(dfs["Power-kW"], errors='coerce')                                                           #Conversion of the data to a numeric type, and filling in NaN values in case it is not a numerical value 
dfs["Power-kW"] = dfs["Power-kW"].fillna(dfs["Power-kW"].mean())                                                            #filling in the NaN values generated from the previous statement

print("Data types of columns after converting their data type")
print(dfs.dtypes)                                                                                                           #checking whether the data types of the columns have got converted or not

print("Summary Statistics")
print(dfs.describe())                                                                                                       #summarizing the data that was converted into numeric type

#Question 2 - c part
##7. Relationship between Cores and Rpeak-TFlop/s using barplot
plt.scatter(dfs['Cores'],dfs['Rmax-TFlop'], color='green', marker='*')                                                      #Creating a scatter plot for Cores versus Rpeak-TFlop/s
plt.title('Cores versus Rmax-TFlop', color='red')
plt.xlabel('Cores', color='blue')
plt.ylabel('Rmax-TFlop', color='blue')

plt.show()

##8. Relationship between Cores and Max_Winds_kt using barplot                                                              #Creating a scatter plot for Cores versus Max_Winds_kt
plt.scatter(dfs['Cores'],dfs['Power-kW'], color='green', marker='*')
plt.title('Cores versus Power-kW', color='red')
plt.xlabel('Cores', color='blue')
plt.ylabel('Power-kW', color='blue')
plt.show()

dfs.to_csv('output.csv', index=False, encoding='utf-8')                                                                     #writing the changes to the file

